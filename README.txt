INTRODUCTION
------------
This module adds the Effective Measure tracking code to pages in your website.
The code is added to the hidden page_bottom region.
* For details on the tracking code and how it is to be implemented:
   https://help.effectivemeasure.com/confluence/display/EH/Audience+Tag+Implementation


CONFIGURATION
-------------
* There are no configuration settings for this module unless the environment
  module is installed, in which case you must select which environments the
  tracking code is to appear on. These settings can be found at:
  Administration » Configuration » System » Effective Measure
